use anyhow::{bail, Result};
use serde_json::{json, Value};
use std::time::SystemTime;

use firestore::{
    FirestoreDb, FirestoreDbOptions, FirestoreListenEvent, FirestoreListenerTarget,
    FirestoreMemListenStateStorage,
};

const TEST_FIRESTORE_PROJECT_ID: &str = "video-analytics-193323";
const TEST_DB_NAME: &str = "mmackliet-signaling-test";
const TEST_COLLECTION_NAME: &str = "systems/system_id/uuid";
const TEST_DOCUMENT_ID: &str = "msg";

async fn write_to_firestore(
    project_id: &str,
    db_name: &str,
    collection: &str,
    doc_id: &str,
    payload: &Value,
    _id_token: &str,
) -> Result<()> {
    // Set up Firestore API endpoint
    let name = format!("projects/{project_id}/databases/{db_name}/documents/{collection}/{doc_id}");
    let firestore_url = format!("https://firestore.googleapis.com/v1/{name}");

    let payload = json!({
        "fields" : payload
    });

    let client = reqwest::Client::new();

    // Build the Firestore API request
    let response = client
        .patch(firestore_url)
        // .header(
        //     reqwest::header::AUTHORIZATION,
        //     format!("Bearer {}", id_token),
        // )
        .header(
            "x-goog-request-params",
            format!("project_id={}&database_id={}", project_id, db_name),
        )
        .json(&payload)
        .send()
        .await?;

    // Check if the request was successful
    if !response.status().is_success() {
        bail!("Error writing document: {}", response.text().await?);
    }

    Ok(())
}

fn average(numbers: &[u128]) -> f64 {
    numbers.iter().sum::<u128>() as f64 / numbers.len() as f64
}

#[tokio::main]
async fn main() -> Result<()> {
    let token = "";
    // let token = std::env::var("ID_TOKEN")?;

    let db = FirestoreDb::with_options(
        FirestoreDbOptions::new(TEST_FIRESTORE_PROJECT_ID.into())
            .with_database_id(TEST_DB_NAME.into()),
    )
    .await
    .unwrap();

    let mut listener = db
        .create_listener(FirestoreMemListenStateStorage::new())
        .await?;

    db.fluent()
        .select()
        .by_id_in(TEST_COLLECTION_NAME)
        .batch_listen([TEST_DOCUMENT_ID])
        .add_target(
            FirestoreListenerTarget::new(rand::random::<u16>().into()),
            &mut listener,
        )?;

    let (tx, mut rx) = tokio::sync::mpsc::channel(1);
    listener
        .start(move |event| {
            let tx = tx.clone();
            async move {
                match event {
                    FirestoreListenEvent::DocumentChange(ref doc_change) => {
                        if let Some(doc) = &doc_change.document {
                            let read_time = SystemTime::now();
                            println!(
                                "{}",
                                serde_json::to_string(
                                    &FirestoreDb::deserialize_doc_to::<Value>(doc).unwrap()
                                )
                                .unwrap()
                            );
                            let _ = tx.send(read_time).await;
                        }
                    }
                    _ => (),
                }

                Ok(())
            }
        })
        .await?;

    let mut round_trip_times = Vec::new();
    for _ in 0..100 {
        let write_time = SystemTime::now();
        let write_time_int = write_time
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_micros();
        write_to_firestore(
            TEST_FIRESTORE_PROJECT_ID,
            TEST_DB_NAME,
            TEST_COLLECTION_NAME,
            TEST_DOCUMENT_ID,
            &json!({"write_time": { "integerValue" : write_time_int}}),
            &token,
        )
        .await?;

        let read_time = rx.recv().await.unwrap();
        let round_trip_ms = read_time.duration_since(write_time).unwrap().as_millis();

        round_trip_times.push(round_trip_ms);
        println!("Round trip: {round_trip_ms}ms");
    }

    let avg_roud_trip = average(round_trip_times.as_slice());
    
    println!("{:?}", round_trip_times);
    println!("Worst case round trip: {}ms", round_trip_times.iter().max().unwrap());
    println!("Best case round trip: {}ms", round_trip_times.iter().min().unwrap());
    println!("Average round trip: {avg_roud_trip}ms");
    println!("Estimated latency: {}ms", avg_roud_trip/2.0);
    listener.shutdown().await?;

    
    Ok(())
}
